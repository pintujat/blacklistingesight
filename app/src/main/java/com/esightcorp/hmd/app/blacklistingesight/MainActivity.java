package com.esightcorp.hmd.app.blacklistingesight;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.esightcorp.hmd.hmd_sound_lib.SoundLink;
import com.esightcorp.hmd.hmd_sound_lib.data.Event;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends BaseActivity implements View.OnClickListener {

    private Button mBtnState, mBtnMove, mBtnPlay, mBtnVol;
    private static final String TAG = "MainActivity";

    @Override
    protected void onResume(){
        super.onResume();
        Log.i(TAG, "onResume: is it pinned? " + KioskModeApp.isInLockMode());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
           /* Log.e(TAG,"It is inside try");
            Runtime runtime=    Runtime.getRuntime();

           // Process process=runtime.exec("dpm set-device-owner com.esightcorp.hmd.app.blacklistingesight/.AdminReceiver");
            Process process=runtime.exec("getenforce");
            Log.i(TAG, "onCreate: "+process.toString());*/
            DevicePolicyManager dpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);

          ;
            Log.i(TAG, "ComponentName: "+  dpm.isDeviceOwnerApp("com.esightcorp.hmd.app.blacklistingesight"));


            try {

                Process process = Runtime.getRuntime().exec("dpm set-device-owner com.esightcorp.hmd.app.blacklistingesight/com.esightcorp.hmd.app.blacklistingesight.AdminReceiver");
                Log.i(TAG, "onCreate: "+process);
                BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

                StringBuffer output = new StringBuffer();

                char[] buffer = new char[4096];
                int read;

                while ((read = reader.read(buffer)) > 0) {
                    output.append(buffer, 0, read);
                }
                Log.i(TAG, "BufferedReader: "+output);
                reader.close();

                process.waitFor();
                BufferedReader reader2 = new BufferedReader(new InputStreamReader(process.getErrorStream()));
                StringBuffer output2 = new StringBuffer();

                char[] buffer2 = new char[4096];
                int read2;

                while ((read2 = reader2.read(buffer2)) > 0) {
                    output2.append(buffer2, 0, read2);
                }
                Log.i(TAG, "output2: "+output2);
                reader.close();
                process.waitFor();
            }

            catch (Exception e)
            {
e.printStackTrace();
            }


        } catch (Exception e) {
            Log.e(TAG,"It is inside catch");
            Log.e(TAG, "device owner not set");
            Log.e(TAG, e.toString());
            e.printStackTrace();
        }

        mBtnState = (Button) findViewById(R.id.btnState);
        mBtnMove = (Button) findViewById(R.id.btnMove);
        mBtnPlay = (Button) findViewById(R.id.btnPlay);
        mBtnVol = (Button) findViewById(R.id.btnVol);

        mBtnState.setOnClickListener(this);
        mBtnMove.setOnClickListener(this);
        mBtnPlay.setOnClickListener(this);
        mBtnVol.setOnClickListener(this);

        setUpAdmin();
        updateButtonState();
    }

    private void updateButtonState() {
        if (KioskModeApp.isInLockMode()) {
            mBtnState.setText("Disable Kiosk Mode");
        } else {
            mBtnState.setText("Enable Kiosk Mode");
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnState) {
            enableKioskMode(!KioskModeApp.isInLockMode());
            updateButtonState();
        } else if (view.getId() == R.id.btnMove) {
            DevicePolicyManager dpm = (DevicePolicyManager) getApplicationContext().getSystemService(Context.DEVICE_POLICY_SERVICE);
//            dpm.wipeData(4);
//            dpm.clearDeviceOwnerApp(getApplicationContext().getPackageName());

            Intent newActivityIntent = new Intent(Intent.ACTION_MAIN);
            newActivityIntent.setComponent(new ComponentName("com.esightcorp.hmd.app.app2","com.esightcorp.hmd.app.app2.MainActivity"));
            Log.i(TAG, "onClick: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            Log.i(TAG, "onClick: is it pinned still? " + KioskModeApp.isInLockMode());
            startActivity(newActivityIntent);

        }else if(view.getId() == R.id.btnPlay){
            Event event1 = new Event("feedback", SoundLink.getInstance(this).getAction(1),
                    Arrays.asList("null"),
                    SystemClock.uptimeMillis(), "LIVE VIEW", 1500,
                    0, true, true);

            SoundLink.getInstance(this).beepBeforeNarrate("ui_loading.ogg",event1);
        }
        else if(view.getId() == R.id.btnVol){
           SoundLink.getInstance(this).increaseNarratorVolume(false);
        }
    }

    @Override
    protected void onStart() {
        Log.i(TAG, "onStart: ");
        super.onStart();
        SoundLink.getInstance(this).connect();
    }

    @Override
    protected void onStop() {
        Log.i(TAG, "onStop: ");
        super.onStop();
        SoundLink.getInstance(this).disconnect();
    }
}
